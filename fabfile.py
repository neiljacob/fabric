import os 
import sys
import datetime
import ConfigParser

from fabric.api import local, run, env, get

BUCKET_NAME = 'urolimetest' #s3 bucket
END_POINT = 'us-east-1' #aws region

env.hosts = [
        '18.219.116.187',
]

env.user = 'ec2-user'
s3_bucket = 'urolimetest'

databases = [
    {
    'user': 'bitbucket-pipelines'
    }
]

time_tag = datetime.datetime.now().strftime('%Y-%m-%d-%H')
dateFormatted=os.system('date -R')

def command():
    run("echo 'test completed'")

